<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyController extends Controller
{
    public function produtos() {
        echo "<h1>Produtos</h1>";
        echo "<ol>";
        echo "<li>manga</li>";
        echo "<li>uva</li>";
        echo "<li>maçã</li>";
        echo "<li>abacaxi</li>";
        echo "</ol>";
    }

    public function getNome() {
        return "José ";
    }

    public function getIdade() {
        return 30;
    }

    public function multiplicar($n1, $n2) {
        return $n1 * $n2;
    }

}
