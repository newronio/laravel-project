<?php

use App\Http\Controllers\MyController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('produtos', [MyController::class, 'produtos']);
Route::get('nome', [MyController::class, 'getNome']);
Route::get('idade', [MyController::class, 'getIdade']);
Route::get('multiplicar/{n1}/{n2}', [MyController::class, 'multiplicar']);

// Route::get('/test', function () {
//     return view('test');
// });

// Route::get('/test1/{name?}', function ($name=null) {

//     if(isset($name))
//     return "Olá " . $name . "!";
//     return "Digita nome ai";
// });

// Route::get('/ruleroute/{name}/{n}', function ($name, $n) {

//     for($i=0; $i<$n; $i++)
//     echo "Olá " . $name . "! <br>";
// })->where('name', '[A-Za-z]+')
//   ->where('n', '[0-9]+');

// Route::prefix('/app')->group(function() {
//     Route::get('/', function(){
//         return view('app');
//     })->name('app');

//     Route::get('/user', function(){
//         return view('user');
//     })->name('app.user');
    
//     Route::get('/profile', function(){
//         return view('profile');
//     })->name('app.profile');
// });

// Route::get('/users', function(){
//     echo "<h1>Usuarios</h1>";
//     echo "<ol>";
//     echo "<li>Joao</li>";
//     echo "<li>Maria</li>";
//     echo "<li>Carlos</li>";
//     echo "<li>Josefino</li>";
//     echo "</ol>";
// })->name('myusers');

// Route::redirect('alluser', 'users', 301);

// Route::get('alluser2', function(){
//     return redirect()->route('myusers');
// });

// Route::get('/requisicoes', function(Request $req){
//     return 'Hello GET';
// });

// Route::post('/requisicoes', function(Request $req){
//     return 'Hello POST';
// });

// Route::delete('/requisicoes', function(Request $req){
//     return 'Hello DELETE';
// });

// Route::put('/requisicoes', function(Request $req){
//     return 'Hello PUT';
// });

// Route::patch('/requisicoes', function(Request $req){
//     return 'Hello PATCH';
// });

// Route::options('/requisicoes', function(Request $req){
//     return 'Hello OPTIONS';
// });